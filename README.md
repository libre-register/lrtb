<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [LrTB](#lrtb)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# LrTB

LrTB (LibreRegister Timetabling) is a rust library for timetabling. It
is currently very early in development, and can be caught out by simple
tricks. It has support for serde, so that timetables can be loaded in on
the fly.
