<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Manual Testing](#manual-testing)
  - [Root Namespace](#root-namespace)
  - [Qualifications](#qualifications)
  - [Students](#students)
  - [Student Bodies](#student-bodies)
  - [Teachers](#teachers)
  - [Timetable](#timetable)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Manual Testing

To Increase Reliability and testing ease I am going to write a
test instead of using randomness. This is because something in the
program's logic is fishy but I don't know what. Plus, this way I
can make the test more difficult.

## Root Namespace

Within the root namespace you need specify the days and sessions, later
used in the timetable.

```toml
days = 3
sessions = 3
```

## Qualifications

Qualifications specify information about a subject.
These can be attached either to a student body, as the qualification
needed for it, or to a teacher, to specify it is a qualification
they have got, and can therefore teach, or to a student, to tell the
program that they need to be in a student body which teaches
such a qualification.

 * *year_7_maths or gcse_physics*

If you want to give a qualification a display name you can do so
as follows:

```toml
[qualifications.year_7_maths]
display_name = "Year 7 Maths"
```


## Students

Students are students who study in the school, and need to take
classes. They cannot teach but can attend classes. They are
efined as follows:

```toml
[students.john_doe]

# This is the display name for the student.
# This is not required but it is recommended to include it.
display_name = "John Doe"

# This is the external ID of the student.
# This is not required but could be helpful in a full
# server environment to link to an ID in sql or to link
# to the student's VCard etc.
external_id = "a4a945fb-db0c-4c40-9350-9a84860eeacf"

# This is a list of qualifications this student needs to take.
# This is not required.
subjects = ["maths", "triple_science"]
```


## Student Bodies

A student body is a pre defined body of students, made up of
multipled different pupils. Each student body has a **single**
qualification attached to it (see above.) These student bodies can
be used for a multitude of things, such as classes, extra curriculim
clubs (not implemented) and such the like.

A student body is defined as followed:

```toml
[student_bodies.body_name]

# This is a list of students by ID. If done through a GUI
# Then these students will probably just be given a generic UUID.
# However there is no limit to what these can be.
students = ["john_doe", "alice_smith", "charlie_brown"]

# This is the qualification for the student body
#
# Not required but if not given then this will not be automatically
# added to a timetable
qualification = "maths"

# This is the display name for this body
display_name = "Extra Curriculim Maths Group"

# This specifies if this should be timetabled and if so how
# to timetable it.
# This should be one of the following values:
# +--------------+---------------------------------------------+
# | "no"         | This should not be automatically timetabled |
# | "fully"      | This should be automatically timetabled.    |
# | "no_teacher" | This should be timetabled without a teacher |
# +--------------+---------------------------------------------+
timetable = "fully"

# This is how many sesions the body has.
# Optional, defaults to 1.
# Only needed if timetabled.
sessions = 1
```

## Teachers

A teacher is defined pretty much the same as a student.
The difference is that qualifications now refer to what the teacher
can teach, rather than what they need to be taught.

```toml
[teachers.mr_doe]

# This is the display name for the Teacher.
# This is not required but it is recommended to include it.
display_name = "Mr Doe"

# This is the external ID of the teacher.
# This is not required but could be helpful in a full
# server environment to link to an ID in sql or to link
# to the student's VCard etc.
external_id = "b03146f3-2e94-4c89-be17-25d0373f1a12"

# This is a list of qualifications the teacher can teach.
# This is required.
qualification = ["maths", "tripple_science"]
```


## Timetable

Within the timetable there is a 3 dimentional array of classes. Each
class contains an optional teacher and a student body.

```toml
[timetable]
arr = [
 [[]], [[{ student_body = "body1", teacher = "john_doe" }]], [[]],
 [[]], [[]], [[]],
 [[]], [[]], [[]],
]
```
