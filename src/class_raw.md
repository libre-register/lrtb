```rust
use serde::Deserialize;
use serde::Serialize;

//For TOML, and i guess JSON, support
#[derive(Deserialize, Serialize, Hash, Eq, PartialEq)]
pub struct ClassRaw {
    #[serde(default)]
    pub teacher: Option<String>,
    pub student_body: String,
}

impl ClassRaw {
    pub fn new(teacher: Option<String>, student_body: String) -> Self {
        Self {
            teacher,
            student_body,
        }
    }
}
```
