```rust
use crate::qualification::Qualification;
use crate::util;
use std::rc::Rc;

/// A structure to store a teacher
#[derive(Debug, Hash, Eq, PartialEq, Clone, Ord, PartialOrd)]
pub struct Teacher {
    pub name: String,
    pub qualifications: Vec<Rc<Qualification>>,
    pub id: String,
}

impl Teacher {
    pub fn new(name: String, qualifications: Vec<Rc<Qualification>>) -> Self {
        Self {
            qualifications,
            id: util::gen_id(&name),
            name,
        }
    }

    pub fn new_with_id(
        name: String,
        qualifications: Vec<Rc<Qualification>>,
        id: String,
    ) -> Self {
        Self {
            name,
            qualifications,
            id,
        }
    }
}
```
