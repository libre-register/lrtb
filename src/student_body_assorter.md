<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Generalised imports](#generalised-imports)
- [Base Struct](#base-struct)
- [New Function](#new-function)
- [Get Body](#get-body)
- [Decepricated: Blacklist](#decepricated-blacklist)
- [Assign Teachers](#assign-teachers)
- [Unaccept](#unaccept)
- [Split](#split)
- [Mask Bodies](#mask-bodies)
- [Check Old](#check-old)

<!-- END doctoc generated TOC please keep comment here to allow auto update
-->

# Generalised imports

```rust
use crate::{Qualification, Student, StudentBody, Teacher};
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryInto;
use std::rc::Rc;
```

# Base Struct

This is the base struct for a student body assorter, it mirrors context
pretty closely as it is more of an extention of context than an individual
struct.

```rust
pub(crate) struct StudentBodyAssorter {
    qualifications: Vec<Rc<Qualification>>,

    students: Vec<Rc<RefCell<Student>>>,
    teachers: Vec<Rc<RefCell<Teacher>>>,

    bodies: Vec<Rc<RefCell<StudentBody>>>,
    all_bodies: Vec<Rc<RefCell<StudentBody>>>,

    teachermap: HashMap<Rc<Qualification>, Vec<Rc<RefCell<Teacher>>>>,
    teacher_modulo_map: HashMap<Rc<Qualification>, usize>,
}
```

# New Function

This creates a new student body assorter, using most of the fields from a
context.

```rust
impl StudentBodyAssorter {
    /// Creates a new version of this.
    pub fn new(
        students: &[Rc<RefCell<Student>>],
        qualifications: &[Rc<Qualification>],
        bodies: &[Rc<RefCell<StudentBody>>],
        teachers: &[Rc<RefCell<Teacher>>],
    ) -> Self {
        let mut nself = Self {
            students: students.to_vec(),
            qualifications: qualifications.to_vec(),
            teachers: teachers.to_vec(),
            bodies: bodies.to_vec(),
            all_bodies: bodies.to_vec(),
            teachermap: HashMap::new(),
            teacher_modulo_map: HashMap::new(),
        };

        // Loops through every qualifications.
        // Adds all the teachers whom can teach this qualification.
        for qualification in &nself.qualifications {
            let mut teachers_clone = nself.teachers.clone();
            teachers_clone.retain(|teacher| {
                teacher.borrow().qualifications.contains(qualification)
            });

            nself
                .teachermap
                .insert(Rc::clone(qualification), teachers_clone);
        }

        nself
    }
```

# Get Body

This function gets a body from the student body assorter so that it can
be timetabled.

```rust
    /// Creates the next student body.
    pub fn get_body(&mut self) -> Option<Rc<RefCell<StudentBody>>> {
        // Get a Qualification to work on.
        let mut option_qualification = None;

        let mut students = Vec::new();

        println!();

        for qualification_candidate in &self.qualifications {
            // Now find all the students which take this qualification and needs
            // a student body to house it in.
            let mut local_students = self.students.clone();
            local_students.retain(|x| {
                x.borrow()
                    .find_unsatisfied_subjects(&self.bodies)
                    .contains(qualification_candidate)
            });

            println!("{} {}", local_students.len(), qualification_candidate.id);

            if !local_students.is_empty() {
                students = local_students;
                option_qualification =
                    Some(Rc::clone(qualification_candidate));
                break;
            }
        }

        let qualification;

        match option_qualification {
            Some(op) => qualification = op,
            None => return None,
        }

        // Sorts them by grades.
        students.sort_unstable_by_key(|x| x.borrow().get_grade(&qualification));

        if students.is_empty() {
            self.qualifications.pop();
            if self.qualifications.is_empty() {
                return None;
            }
            return self.get_body();
        }

        // Creates a body.
        let mut body = StudentBody::new(
            Rc::clone(&qualification),
            qualification.sessions.try_into().unwrap(),
            crate::util::gen_id(&qualification.id),
        );

        // Picks some students to be in the body.
        for _ in 0..qualification.optimum_class_size {
            let student = students.pop();
            if student.is_none() {
                break;
            }
            let student = student.unwrap();

            body.students.push(Rc::clone(&student));
        }

        // Now we need to check if we have already made this body.
        let mut possible = self.all_bodies.clone();
        possible.retain(|x| {
            x.borrow().qualification == body.qualification
                && x.borrow().students == body.students
        });

        if possible.is_empty() {
            self.assign_teacher(&mut body);
            body.generated = true;

            let body = Rc::new(RefCell::new(body));
            self.bodies.push(Rc::clone(&body));
            self.all_bodies.push(Rc::clone(&body));
            Some(body)
        } else {
            possible.last().unwrap().borrow_mut().filled_sessions = 0;
            self.bodies.push(possible.last().unwrap().clone());
            possible.last().map(Rc::clone)
        }
    }
```

# Decepricated: Blacklist

This function was going to be included, but it has not. Its
functionality would have been simmilar to what is now
[Unaccept](#unaccept).

```rust
    /*    /// Swaps a student out for the next best, marking it as to not be
        /// put in that body again.
        pub fn blacklist(
            &mut self,
            student: Rc<RefCell<Student>>,
            body: Rc<RefCell<StudentBody>>,
            havoc_record: StudentHavocRecord,
        ) {
        }
    */
```

# Assign Teachers

This function gets a student body and gives it some teachers. This is
generally helpful and is called from [Get Body](#get_body).

```rust
    /// Assigns a teacher to a student body.
    fn assign_teacher(&mut self, student_body: &mut StudentBody) {
        if self
            .teacher_modulo_map
            .get(&student_body.qualification)
            .is_none()
        {
            self.teacher_modulo_map
                .insert(Rc::clone(&student_body.qualification), 0);
        }

        // Get the teachers for this qualification
        let teachers = &self.teachermap[&student_body.qualification];

        let modulo = self.teacher_modulo_map[&student_body.qualification];

        // Checks the student body doesn't already have teachers.
        if student_body.teachers.is_empty() {
            // Adds them to the student body
            student_body
                .teachers
                .append(&mut teachers[modulo..teachers.len()].to_owned());
            if modulo != 0 {
                student_body
                    .teachers
                    .append(&mut teachers[0..modulo].to_owned());
            }
        }

        // Increase the modulo hashmap
        self.teacher_modulo_map.insert(
            Rc::clone(&student_body.qualification),
            (self
                .teacher_modulo_map
                .get(&student_body.qualification)
                .unwrap()
                + 1)
                % teachers.len(),
        );
    }
```

# Unaccept

Marks a body, which was generated by this, as unaccepted. This, in
short, means that this body caused clashes and so it needs to be looked
at to try and repair. Normally after doing this you will call
[Split](#split)

```rust
    pub(crate) fn unaccept(
        &mut self,
        body: Rc<RefCell<StudentBody>>,
    ) -> Option<Rc<RefCell<StudentBody>>> {
        self.bodies.retain(|x| x != &body);

        let mut applicable = self.bodies.clone();
        applicable.retain(|x| {
            x.borrow().qualification == body.borrow().qualification &&
            x.borrow().sessions == body.borrow().sessions
        });

        // Work out which body we need to split with.
        applicable.last().map(Rc::clone)
    }
```

# Split

This gets two student bodies and mixes their students together, getting
a

```rust
    pub(crate) fn split(
        &mut self,
        bodies: Vec<Rc<RefCell<StudentBody>>>,
    ) -> Vec<Rc<RefCell<StudentBody>>> {
        println!("<SPLIT IN> {:?} </SPLIT IN>", bodies);
        self.bodies.retain(|x| !bodies.contains(x));

        // Sort all student bodies students by grades.
        for body in &bodies {
            //println!("{:#?}", body.borrow().students.iter().map(|x|
            // x.borrow().id.clone()).collect::<Vec<String>>());
            let qualification = Rc::clone(&body.borrow().qualification);

            body.borrow_mut()
                .students
                .sort_unstable_by_key(|x| x.borrow().get_grade(&qualification))
        }

        // New bodies.
        let mut new_bodies = Vec::with_capacity(bodies.len());

        for body in &bodies {
            new_bodies.push(StudentBody::new(
                Rc::clone(&body.borrow().qualification),
                body.borrow().sessions,
                crate::util::gen_id(&body.borrow().qualification.id),
            ));
        }

        // Add students
        let mut i = 0;
        for body in &bodies {
            for student in &body.borrow().students {
                new_bodies[i].students.push(Rc::clone(student));
                i += 1;
                i %= bodies.len();
            }
        }

        for (i, body) in bodies.iter().enumerate() {
            new_bodies[i].teachers = body.borrow().teachers.clone()
        }

        // Return
        let rc_bodies = new_bodies
            .into_iter()
            .map(|x| Rc::new(RefCell::new(x)))
            .collect();

        for body in &rc_bodies {
            self.bodies.push(Rc::clone(body));
            self.all_bodies.push(Rc::clone(body));
        }

        println!("<SPLIT OUT> {:?} </SPLITOUT>", rc_bodies);

        rc_bodies
    }
```

# Mask Bodies

This function gets rid of bodies which aren't needed, as to not get
confused and accidentaly give a student less sessions than need be.

```rust
    /// Makes sure all our bodies are included in `vec`
    pub fn mask_bodies(&mut self, vec: &[Rc<RefCell<StudentBody>>]) {
        self.bodies.retain(|x| vec.contains(x));
    }
}
```
