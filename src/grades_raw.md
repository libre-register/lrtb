```rust
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash,
)]
/// A grade stores an individual grade in a student, so that it can be
/// put in relatively good student bodies.
pub struct GradeRaw {
    pub qualification: String,
    /// This is a number between 0 and 1,000,000,000 (because floats are
    /// too inaccurate)
    pub grade: u32,
}

impl GradeRaw {
    pub fn new(qualification: String, grade: u32) -> Self {
        Self {
            qualification,
            grade,
        }
    }
}
```
