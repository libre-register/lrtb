```rust
use crate::ClassRaw;
use serde::Deserialize;
use serde::Serialize;

//For TOML, and i guess JSON, support
#[derive(Deserialize, Serialize)]
pub struct TimeSlotRaw {
    pub classes: Vec<ClassRaw>,
}

impl TimeSlotRaw {
    pub fn new(classes: Vec<ClassRaw>) -> Self {
        Self { classes }
    }
}
```
