# Constrains

The idea with constrains is to generalise as much as possible, that is you
have one constraint to check for clashes, and another for teachers, etc.

## Imports

```rust
use crate::{Class, StudentBody, TimeSlot};
use std::cell::RefCell;
use std::rc::Rc;
```

## The Trait

With the constraint trait, the constraint will be given the following
information:

 * `StudentBody` which we are working on.
 * `TimeSlot` we are looking at.

to infer from.

```rust
pub trait Constraint {
    /// Assume that `1` is the clashing that occurs with one student.
    #[must_use = "If you are going to run a check, use its value."]
    fn check(
        &mut self,
        student_body: &StudentBody,
        timeslot: &TimeSlot,
        excluded: &[Rc<RefCell<Class>>],
    ) -> (f64, Vec<Rc<RefCell<Class>>>);
}
```

## Student Constraint

This is the constraint which will occur when a student has two lessons on
the same timeslot. This will add 1 to the clash.

```rust
#[derive(Clone, Copy, Default)]
pub struct StudentConstraint;

impl Constraint for StudentConstraint {
    fn check(
        &mut self,
        student_body: &StudentBody,
        timeslot: &TimeSlot,
        excluded: &[Rc<RefCell<Class>>],
    ) -> (f64, Vec<Rc<RefCell<Class>>>) {
        let mut clashes = 0_f64;
        let mut classes = Vec::new();

        for class in &timeslot.classes {
            if excluded.contains(class) {
                continue;
            }
            for student in &class.borrow().student_body.borrow().students {
                for our_student in &student_body.students {
                    if Rc::ptr_eq(our_student, student) {
                        clashes += 1_f64;
                        classes.push(Rc::clone(class));
                    }
                }
            }
        }

        classes.dedup();

        (clashes, classes)
    }
}
```

## Teacher Constraint

This constraint adds to the clashes when there is not a single teacher
applicable for the timeslot, as all would be double booked. It will give a
clash of the amount of students in the class.

```rust
#[derive(Clone, Copy, Default)]
pub struct TeacherConstraint;

impl Constraint for TeacherConstraint {
    fn check(
        &mut self,
        student_body: &StudentBody,
        timeslot: &TimeSlot,
        excluded: &[Rc<RefCell<Class>>],
    ) -> (f64, Vec<Rc<RefCell<Class>>>) {
        let mut teachers = student_body.teachers.clone();
        let mut fail = 0_f64;

        if teachers.is_empty() {
            return (0.0, Vec::new());
        }

        for class in &timeslot.classes {
            if excluded.contains(class) {
                continue;
            }
            teachers.retain(|x| Some(x) != class.borrow().teacher.as_ref());
            fail += class.borrow().student_body.borrow().students.len() as f64;
        }

        // Checks if its empty or not
        if teachers.is_empty() {
            (fail, timeslot.classes.clone())
        } else {
            (0.0, Vec::new())
        }
    }
}
```
