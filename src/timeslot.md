```rust
use crate::error::TablingError;
use crate::Class;
use crate::Student;
use crate::StudentBody;
use std::cell::RefCell;
use std::rc::Rc;

/// Stores a individual timeslot, with everyone's classes there.
#[derive(Debug, Eq, PartialEq, Clone)]
pub struct TimeSlot {
    pub classes: Vec<Rc<RefCell<Class>>>,
    pub(crate) excluded_bodies: Vec<Rc<RefCell<StudentBody>>>,
    pub day: u64,
    pub session: u64,
}

impl TimeSlot {
    pub fn new(day: u64, session: u64) -> Self {
        Self {
            day,
            session,
            classes: Vec::new(),
            excluded_bodies: Vec::new(),
        }
    }

    /// Removes all excluded bodies
    pub fn remove_excluded_bodies(&mut self) {
        self.excluded_bodies.clear();
    }

    /// Gets all the sessions for a student
    pub fn give_students_sessions(
        &self,
        student: &Rc<RefCell<Student>>,
    ) -> Vec<Rc<RefCell<Class>>> {
        // To store the classes
        let mut classes = Vec::new();

        // Loops through the class
        for class in &self.classes {
            if class
                .borrow()
                .student_body
                .borrow()
                .students
                .contains(student)
            {
                classes.push(class.clone());
            }
        }
        classes
    }

    /// Removes a class from this.
    pub fn remove_class(
        &mut self,
        class: &Rc<RefCell<Class>>,
    ) -> Result<(), TablingError> {
        /*        println!(
            "{:#?}\n{}\n",
            class.borrow().id,
            std::backtrace::Backtrace::capture().to_string().split("\n").collect::<Vec<&str>>().len() / 2
        );*/

        // Have we removed it?
        let mut removed = false;

        for uclass in &self.classes {
            if uclass == class {
                class.borrow().student_body.borrow_mut().filled_sessions -= 1;
                removed = true;
                class.borrow().student_body.borrow_mut().difference -= 1;
            }
        }
        self.classes.retain(|uclass| class != uclass);

        if !removed {
            return Err(TablingError::ClassNotFound);
        }

        Ok(())
    }

    /// Adds a class to this.
    pub fn add_class(
        &mut self,
        class: &Rc<RefCell<Class>>,
    ) -> Result<(), TablingError> {
        // Checks the class hasn't already been added somewhere.
        if class.borrow().slot.is_some() {
            return Err(TablingError::ClassAlreadyAdded);
        }

        // Clones the class to get a clean Rc.
        let class = Rc::clone(class);

        // Adds this to the `new_ses` in `StudentBody`
        class.borrow().student_body.borrow_mut().difference += 1;

        class.borrow_mut().slot = Some((self.day, self.session));

        class.borrow().student_body.borrow_mut().filled_sessions += 1;

        // Adds the class to this
        self.classes.push(class);

        Ok(())
    }

    pub fn get_body_classes(
        &self,
        body: &Rc<RefCell<StudentBody>>,
    ) -> Option<Rc<RefCell<Class>>> {
        // Tries to find the body
        for class in &self.classes {
            if Rc::ptr_eq(&class.borrow().student_body, body) {
                return Some(Rc::clone(class));
            }
        }
        // Return `None` on failure
        None
    }
}
```
