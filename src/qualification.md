```rust
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

/// A structure to store a qualification a teacher has.
#[derive(
    Debug, Hash, Eq, PartialEq, Ord, PartialOrd, Clone, Serialize, Deserialize,
)]
pub struct Qualification {
    pub id: String,
    pub display_name: Option<String>,
    pub optimum_class_size: usize,
    pub sessions: usize,
}

impl Qualification {
    pub fn new(id: String, display_name: Option<String>) -> Self {
        Qualification {
            id,
            display_name,
            optimum_class_size: 1,
            sessions: 1,
        }
    }
}
impl Display for Qualification {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.display_name.as_ref().unwrap_or(&self.id))
    }
}
```
