<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Context](#context)
  - [Imports](#imports)
  - [Setup](#setup)
  - [Functions](#functions)
    - [New Function](#new-function)
  - [Check Valid Student Bodies](#check-valid-student-bodies)
    - [Revert](#revert)
    - [Generation](#generation)
      - [Gen](#gen)
      - [Gen1](#gen1)
  - [Context Raw](#context-raw)
  - [Small structs](#small-structs)
    - [Gen 1 Results](#gen-1-results)
    - [Revert Result](#revert-result)

<!-- END doctoc generated TOC please keep comment here to allow auto update
--> -->

# Context

A context is the base object for the base object for all timetabling. It
contains all the students, bodies, and the timetable, and will timetable
accordingly. It also preforms the timetabling.

## Imports

Just import some prerequisites.

```rust
use crate::error::*;
use crate::student_body_assorter::StudentBodyAssorter;
use crate::Class;
use crate::ContextRaw;
use crate::Grade;
use crate::Qualification;
use crate::Student;
use crate::StudentBody;
use crate::Teacher;
use crate::Timetable;
use crate::MAX_RECURSION;
use rand::seq::SliceRandom;
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::error::Error;
use std::rc::Rc;
```

## Setup

This is the base struct, contains all the fields needed for sufficient
timetabling.

```rust
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Context {
    pub teachers: Vec<Rc<RefCell<Teacher>>>,
    pub days: u64,
    pub sessions: u64,

    pub student_bodies: Vec<Rc<RefCell<StudentBody>>>,
    pub students: Vec<Rc<RefCell<Student>>>,
    pub timetable: Option<Timetable>,
    pub qualifications: Vec<Rc<Qualification>>,
    //prev_percent: f64,
}
```

## Functions

These are the functions associated with the context.

```rust
impl Context {
```
### New Function

The `new` function literally just takes days and sessions, and
automatically fills in everything else using `Default::default()`

```rust
    /// Creates a new Context
    pub fn new(days: u64, sessions: u64) -> Self {
        Context {
            teachers: Vec::new(),
            days,
            sessions,
            student_bodies: Vec::new(),
            students: Vec::new(),
            timetable: None,
            qualifications: Vec::new(),
            // prev_percent: 0.0,
        }
    }
```

## Check Valid Student Bodies

During the algorithm, it is important to check that student bodies
haven't got more sessions than are in the timetable. Take the following
example:

 * The context has a 3x3 week.
 * Mary takes 3 science lessons, 4 maths lessons and 3 english lessons.
 * The program returns an error because this is not possible.

During the timetabling process, if a student body is generated through a
`StudentBodyAssorter` than this also becomes valuable for debugging,
because it can help find obvious problems with the algorithm.

```rust
    /// Checks that no student has too many sessions in the week. If they do
    /// then it will return the first student it finds.
    pub fn check_valid_student_sessions(
        &self,
    ) -> Result<(), Rc<RefCell<Student>>> {
        // Hashmap to store how many each student has.
        let mut hashmap: HashMap<String, i32> = HashMap::new();

        // Now we check if any students have too many classes.
        // If so we exit.
        for body in &self.student_bodies {
            for student in &body.borrow().students {
                // Adds the bodies sessions
                hashmap.insert(
                    student.borrow().id.clone(),
                    hashmap.get(&student.borrow().id).unwrap_or(&0)
                        + body.borrow().sessions,
                );

                // Checks if we have sucseeded the session amount.
                if hashmap[&student.borrow().id]
                    > (self.days * self.sessions) as i32
                {
                    return Err(student.clone());
                }
            }
        }
        Ok(())
    }
```

### Revert

During the timetable algorithm, because of the mannor of how it works,
reverting is often necessary. This involves going back to a previous
stage in the generation process. The way this function works is simple:
 * Checks if there is a timetable to go back to. If so go back to it,
   else return an error.
 * Gets a vector of all the student bodies.
 * Shuffle it so that in cases of non-generated student bodies
   re-timetabling can become easier.
 * Fix how many filled sessions student bodies have, to fix any possible
   empty slots.
 * Make sure that everything in the vector still has classes it needs
   timetabled.
 * Give back the vecor, along with how many sessions are filled.

```rust
    /// Reverts us to a previous stage
    fn revert(
        &mut self,
        revertables: &mut Vec<Timetable>,
        error: Box<dyn Error>,
    ) -> Result<RevertResult, Box<dyn Error>> {
        self.timetable = Some(match revertables.pop() {
            Some(timetable) => timetable,
            None => return Err(error),
        });
        let mut vec = self.student_bodies.clone();
        vec.shuffle(&mut rand::thread_rng());

        let mut filled_sessions = 0_i32;

        self.timetable
            .as_ref()
            .unwrap()
            .correct_filled_sessions(&mut self.student_bodies);
        vec.retain(|x| x.borrow().filled_sessions != x.borrow().sessions);

        for body in self.timetable.as_ref().unwrap().get_bodies() {
            filled_sessions += i32::try_from(body.borrow().students.len())
                .unwrap()
                * body.borrow().sessions;
        }

        Ok(RevertResult {
            student_bodies: vec,
            filled_sessions,
        })
    }
```

### Generation

Within the generation there are two functions, `gen` (public) and `gen1`
(private.) The difference between them is that gen is a non-recursive
function which uses `gen1`

#### Gen

Gen works as follows:

 * Enter loop, we do not leave this loop until done.
 * Check if there are any manually specified student bodies:
     * If so then run a gen1, storing the result in the variable
       `result`
     * If not then create a new student body using the
       [`StudentBodyAssorter`](./student_body_assorter.md).
     * If the body is `None` then we are done, return out of the
       function.
     * If not then we will unwrap the body and add it to out list of
       student bodies.
     * Then we run a `gen1` and store it in the variable `result`
 * Check wether we have had a sucsess or not (wether the amount of
   classes added and removed in `gen1` is greater than 0.)
     * If so then commit this timetable to the list of timetables (for
       reverting purposes). We also check if there are any fully
       excluded student bodies and, if so, we try a revert.
     * If not then we try and run a revert.

```rust
    /// Generates a timetable with a heuristic approach.
    ///
    /// Update function here refers to a function to be used to state
    /// the progress of the timetabling. The Timetable given is an
    /// immutable reference so you can't mess with it, and the f64
    /// is a value in between 0 and 1.
    pub fn gen(
        &mut self,
        update_function: Option<&dyn Fn(&Context, f64)>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Vector of prespecified student bodies.
        let mut vec;

        // Creates a timetable
        self.timetable = Some(Timetable::new(self.days, self.sessions));

        // How many sessions are their to fill?
        let mut sessions = 0_i32;
        for student in &self.students {
            for qualification in student
                .borrow()
                .find_unsatisfied_subjects(&self.student_bodies)
            {
                sessions += i32::try_from(qualification.sessions).unwrap();
            }
        }
        for body in &self.student_bodies {
            sessions += body.borrow().sessions
                * i32::try_from(body.borrow().students.len()).unwrap();
        }

        // Lock down the variable to an immutable state.
        let sessions = sessions;

        // How many sessions have we filled?
        let mut filled_sessions = 0_i32;

        // Creates some backup timetables
        let mut revertables = vec![self.timetable.clone().unwrap()];

        let mut student_body_assorter =
            crate::student_body_assorter::StudentBodyAssorter::new(
                &self.students,
                &self.qualifications,
                &self.student_bodies,
                &self.teachers,
            );

        loop {
            self.student_bodies.sort();
            self.student_bodies.dedup();
            vec = self.student_bodies.clone();
            vec.retain(
                |x| x.borrow().sessions > x.borrow().filled_sessions
            );

            if let Some(update_function) = update_function {
                // And if so call it
                update_function(
                    self,
                    f64::from(filled_sessions) / f64::from(sessions),
                );
            }

            // Run tests, to make sure we haven't reached an impossible state.
            if let Err(e) = self.check_valid_student_sessions() {
                return Err(Box::new(TablingError::TooManySessions(e)));
            }

            if !vec.is_empty() {
                if let Some(result) =
                    self.gen1(&mut vec, 0, &mut student_body_assorter)?
                {
                    if result.difference <= 0 {
                        println!("REVERT MODE ENABELD");
                        //  Run a revert
                        let revert_result = self.revert(
                            &mut revertables,
                            Box::new(TablingError::NoMoreReverts),
                        )?;

                        filled_sessions = revert_result.filled_sessions;

                        // Exclude this body
                        self.timetable.as_mut().unwrap().arr
                            [result.slot.0 as usize]
                            [result.slot.1 as usize]
                            .excluded_bodies
                            .push(result.body);
                            
                        revertables.pop();
                    } else {
                        // Checks if we have any fully excluded bodies
                        if let Some(body) = self
                            .timetable
                            .as_mut()
                            .unwrap()
                            .check_fully_excluded(&self.student_bodies)
                        {
                            let result = self.revert(
                                &mut revertables,
                                Box::new(TablingError::FullyExcludedBody(body)),
                            )?;

                            self.student_bodies = result.student_bodies;
                            filled_sessions = result.filled_sessions;
                            student_body_assorter.mask_bodies(
                                &self.timetable.as_ref().unwrap().get_bodies(),
                            );
                        } else {
                            // Commit the timetable
                            revertables
                                .push(self.timetable.as_ref().unwrap().clone());

                            // Increase the filled sessions by the difference,
                            // this will likely be a small number, always
                            // smaller than MAX_RECURSION
                            filled_sessions += result.difference;

                            // Check if the bdoy has filled its quote
                            if vec.last().unwrap().borrow().filled_sessions
                                == vec.last().unwrap().borrow().sessions
                            {
                                vec.pop();
                                self.timetable
                                    .as_mut()
                                    .unwrap()
                                    .remove_excluded_bodies();
                            }
                        }
                    }
                }
            } else {
                // Give the student body the correct information.
                student_body_assorter.mask_bodies(
                    &self.timetable.as_ref().unwrap().get_bodies(),
                );

                // Create a body who we can timetable
                let body = student_body_assorter.get_body();

                // Check if the body is exists. If it exists then we may
                // continue, otherwise we are finished.
                if let Some(body) = body {
                    self.student_bodies.push(Rc::clone(&body));
                } else {
                    break;
                }
            }
        }
        Ok(())
    }
```

#### Gen1

For a lack of better terms, gen1 preforms a single, simple stage of the
generation process. It works by trying to place a class in and, if it
cannot, it finds the best slot, removing old classes, and recursing.


It returns a tuple which stores:
 * The difference in placed student bodies before and after this was
   run.
 * The position on the timetable which was tested. This is to be used to
   exclude bodies and such the like.

```rust
    /// A single stage of the generation process
    ///
    /// It returns a tuple which stores:
    ///     1. The difference in placed student bodies before and
    ///         after this was run.
    ///     2. The position on the timetable which was tested.
    ///         This is to be used to exclude bodies and such the like.
    fn gen1(
        &mut self,
        vec: &mut Vec<Rc<RefCell<StudentBody>>>,
        recurse_level: u8,
        student_body_assorter: &mut StudentBodyAssorter,
    ) -> Result<Option<Gen1Result>, Box<dyn std::error::Error>> {
        // Checks if we have recursed far enough.
        if recurse_level > MAX_RECURSION {
            // Return a statement saying we did nothing.
            return Ok(None);
        }

        // Select a body which still has class left in it
        let body = Rc::clone(vec.last().unwrap());

        // How many sessions we have added, when displacing this
        // decreases and when adding this increases.
        let mut difference = 0;

        // Gets the timetable from us for easier use.
        let timetable = self.timetable.as_mut().unwrap();

        // Find the best slot to place it in and place it in the slot.
        // If there are not pefrect slots then we displace classes and
        // begin recursion. The algorithm used here is taken from FET,
        // although is not a direct rewrite.
        let mut slot = body.borrow().get_least_clashes(timetable);

        // Now we need to add it to the best slot. This involves creating
        // a class.
        // TODO: Remove the needed complesxity of the reference with
        // TODO: creating classes.
        let mut class = Class::new(&body);
        class.teacher = slot.teacher;

        let class = Rc::new(RefCell::new(class));

        timetable.arr[slot.i as usize][slot.j as usize].add_class(&class)?;
        // Increment the difference
        difference += 1;

        // Stores how many corrective `gen1`s we should call.
        let mut to_call = Vec::new();

        slot.clashes.dedup();

        // Now if there are any clashes which
        // need to be displaced we loop though them.
        for clash in &slot.clashes {
            to_call.push(Rc::clone(&clash.borrow().student_body));

            // Displace the clash
            timetable.arr[slot.i as usize][slot.j as usize]
                .remove_class(clash)?;
            // Decrease the difference
            difference -= 1;

            // Checks if it needs to be re added to the list.
            vec.retain(|x| Rc::ptr_eq(x, &clash.borrow().student_body));
            vec.push(Rc::clone(&clash.borrow().student_body));
        }

        let mut skip = Vec::new();

        for clashing_body in to_call {
            // Only for if the clashing_body has been on-the-fly generated
            if clashing_body.borrow().generated &&
                !skip.contains(&clashing_body) &&
                recurse_level == 0
                {
                
                // Borrow timetable so the borrow checker plays nice.
                let timetable = self.timetable.as_mut().unwrap();

                let unaccepted =
                    student_body_assorter.unaccept(Rc::clone(&clashing_body));

                if let Some(unaccepted) = unaccepted {
                    if unaccepted != body {
                        // Remove the bodies from lists
                        self.student_bodies.retain(|x| x != &unaccepted);
                        vec.retain(
                            |x| x != &unaccepted
                        );
                        self.student_bodies.retain(
                            |x| x != &clashing_body
                        );
                        vec.retain(|x| x != &clashing_body);

                        // Remove them from the timetable.
                        timetable.remove_body(&unaccepted)?;
                        timetable.remove_body(&clashing_body)?;

                        // Make sure we don't double allocate any classes.
                        skip.push(Rc::clone(&unaccepted));

                        // And split
                        let mut new_bodies =
                            student_body_assorter.split(vec![clashing_body, unaccepted]);
                        self.student_bodies.append(&mut new_bodies.clone());
                        vec.append(&mut new_bodies);
                    }
                }
            }

            // And try some recursion
            let result =
                self.gen1(vec, recurse_level + 1, student_body_assorter)?;
            // Modify the difference
            difference += match result.as_ref() {
                Some(result) => result.difference,
                None => 0,
            };
        }

        // Returns the result of this ordeal
        Ok(Some(Gen1Result::new(difference, (slot.i, slot.j), body)))
    }
}
```

## Context Raw

This function allows the conversion from context raw, to context.

```rust
// For Conversion
impl TryFrom<ContextRaw> for Context {
    type Error = Box<dyn std::error::Error>;

    // The Conversion Function.
    fn try_from(mut context: ContextRaw) -> Result<Context, Self::Error> {
        // Create the base context
        let mut nself = Self::new(context.days, context.sessions);

        // HashMap to store the qualifications.
        let mut qualifications = HashMap::new();

        // Loop through the qualifications
        for (k, qualification) in context.qualifications.drain() {
            qualifications.insert(
                k.clone(),
                Rc::new(Qualification {
                    id: k,
                    display_name: Some(qualification.display_name.clone()),
                    optimum_class_size: qualification.optimum_class_size,
                    sessions: qualification.sessions,
                }),
            );
        }

        // Hashmap to store the teachers, this will later be turned
        // into just the values.
        let mut teachers = HashMap::new();

        // Loop through the teachers
        for (k, mut teacher) in context.teachers.drain() {
            // Insert the new teacher.
            teachers.insert(
                k.clone(),
                Rc::new(RefCell::new(Teacher::new_with_id(
                    teacher.name,
                    // Have to convert these to and from
                    {
                        // Vector
                        let mut vec = Vec::new();

                        // Loop
                        for subject in teacher.qualifications.drain(..) {
                            vec.push(Rc::clone(
                                match &qualifications.get(&subject) {
                                    Some(s) => s,
                                    None => return Err(Box::new(
                                        ContextRawToContextError::KeyNotFound(
                                            format!("qualifications.{subject}"),
                                        ),
                                    )),
                                },
                            ))
                        }
                        vec
                    },
                    k,
                ))),
            );
        }

        // Hashmap to store the students, this will later be turned
        // into just the values.
        let mut students = HashMap::new();

        // Loop through the students
        for (k, mut student) in context.students.drain() {
            // Insert the new student.
            students.insert(
                k.clone(),
                Rc::new(RefCell::new({
                    let mut new_student = Student::new_with_id(
                        student.name,
                        // Have to convert these to and from
                        {
                            // Vector
                            let mut vec = Vec::new();

                            // Loop
                            // DONE: Fix this with Rcs
                            for subject in student.subjects.drain(..) {
                                vec.push(Rc::clone(match &qualifications.get(&subject) {
                                    Some(s) => s,
                                    None => return Err(Box::new(ContextRawToContextError::KeyNotFound(format!("qualifications.{subject}")))),
                                }))
                            }
                            vec
                        },
                        k,
                    );

                    new_student.grades = {
                        // Vector for the new grades
                        let mut grades = Vec::new();

                        // Loop through all the grades.
                        for grade in student.grades.into_iter() {
                            grades.push(Grade::new(
                                Rc::clone(match &qualifications.get(&grade.qualification) {
                                    Some(s) => s,
                                    None => return Err(Box::new(ContextRawToContextError::KeyNotFound(format!("qualifications.{}", grade.qualification)))),
                                }),
                                grade.grade,
                            ));
                        }

                        grades
                    };

                    new_student
                })),
            );
        }

        // Hashmap to store all the student bodies.
        let mut bodies = HashMap::new();

        // Loops through all the bodies
        for (k, body) in context.student_bodies.drain() {
            // Creates a new body
            let mut nbody = StudentBody::new(
                Rc::clone(match &qualifications.get(&body.qualification) {
                    Some(s) => s,
                    None => {
                        return Err(Box::new(
                            ContextRawToContextError::KeyNotFound(format!(
                                "qualifications.{}",
                                body.qualification
                            )),
                        ))
                    }
                }),
                body.sessions,
                k.clone(),
            );

            // Gets the students and adds them to it.
            for student in &body.students {
                nbody.students.push(
                    match students.get(student) {
                        Some(s) => s,
                        None => {
                            return Err(Box::new(
                                ContextRawToContextError::KeyNotFound(format!(
                                    "students.{}",
                                    student
                                )),
                            ))
                        }
                    }
                    .clone(),
                );
            }
            // Gets the students and adds them to it.
            for teacher in &body.teachers {
                nbody.teachers.push(
                    match teachers.get(teacher) {
                        Some(s) => s,
                        None => {
                            return Err(Box::new(
                                ContextRawToContextError::KeyNotFound(format!(
                                    "teachers.{}",
                                    teacher
                                )),
                            ))
                        }
                    }
                    .clone(),
                );
            }

            // Adds it to the array.
            // Also turns it into an RC refcell, so later it
            // can be referenced multiple times.
            bodies.insert(k, Rc::new(RefCell::new(nbody)));
        }

        // Checks if there even is a timetable, likilihood is that there
        // is.
        if let Some(mut timetable) = context.timetable {
            // Create a new non raw timetable.
            let mut ntimetable = Timetable::new(context.days, context.sessions);

            // Begins to loop through the timetable
            for (day, dayi) in timetable.arr.iter_mut().enumerate() {
                for (session, sessioni) in dayi.iter_mut().enumerate() {
                    for class in sessioni.classes.drain(..) {
                        ntimetable.arr[day][session].add_class(&Rc::new(
                            RefCell::new(Class::new(
                                match &bodies.get(&class.student_body) {
                                    Some(s) => s,
                                    None => return Err(Box::new(
                                        ContextRawToContextError::KeyNotFound(
                                            format!(
                                                "student_bodies.{}",
                                                &class.student_body
                                            ),
                                        ),
                                    )),
                                },
                            )),
                        ))?;
                    }
                }
            }

            nself.timetable = Some(ntimetable)
        }

        // Adds all the hashmaps to the context.
        //
        // This code was taken from a handy StackOverFlow
        //
        // Qualifications
        nself.qualifications =
            qualifications.into_iter().map(|(_id, val)| val).collect();
        // Teachers
        nself.teachers = teachers.into_iter().map(|(_id, val)| val).collect();
        // Students
        nself.students = students.into_iter().map(|(_id, val)| val).collect();
        // Bodies
        nself.student_bodies =
            bodies.into_iter().map(|(_id, val)| val).collect();
        Ok(nself)
    }
}
```

## Small structs

### Gen 1 Results

This is a small struct which acts to store the results of running `gen`.

```rust
#[derive(Debug, Clone)]
struct Gen1Result {
    difference: i32,
    slot: (u64, u64),
    body: Rc<RefCell<StudentBody>>,
}

impl Gen1Result {
    fn new(
        difference: i32,
        slot: (u64, u64),
        body: Rc<RefCell<StudentBody>>,
    ) -> Self {
        Self {
            difference,
            slot,
            body,
        }
    }
}
```

### Revert Result

Another small struct, just stores the results of running a gen 1.

```rust
struct RevertResult {
    student_bodies: Vec<Rc<RefCell<StudentBody>>>,
    filled_sessions: i32,
}
```
