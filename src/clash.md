```rust
use std::cell::RefCell;
use std::cmp::Ordering;
use std::ops::{Add, AddAssign};
use std::rc::Rc;

use crate::Class;
use crate::Teacher;

#[derive(Debug, Clone, PartialEq)]
pub struct ClashRecord {
    pub i: u64,
    pub j: u64,
    pub best: f64,
    pub clashes: Vec<Rc<RefCell<Class>>>,
    pub teacher: Option<Rc<RefCell<Teacher>>>,
}

impl ClashRecord {
    pub fn new() -> Self {
        ClashRecord {
            i: 0,
            j: 0,
            best: f64::MAX,
            clashes: Vec::new(),
            teacher: None,
        }
    }
}

// Allows the concatenation of two clashrecords.
impl AddAssign for ClashRecord {
    /// Adds two clash records together.
    ///
    /// # Panics
    ///
    /// Panics if the Clashrecords have different `i` and `j` values.
    fn add_assign(&mut self, mut other: Self) {
        assert_eq!(self.i, other.i);
        assert_eq!(self.j, other.j);

        self.best += other.best;
        self.clashes.append(&mut other.clashes);

        // If we have a teacher, keep out teacher, otherwise use
        // `other`s teacher
        if self.teacher.is_none() {
            self.teacher = other.teacher.clone()
        }
    }
}

// Allows the concatenation of two clashrecords.
impl Add for ClashRecord {
    type Output = Self;

    /// Adds two clash records together.
    ///
    /// # Panics
    ///
    /// Panics if the Clashrecords have different `i` and `j` values.
    fn add(mut self, mut other: Self) -> Self {
        assert_eq!(self.i, other.i);
        assert_eq!(self.j, other.j);

        Self {
            i: self.i,
            j: self.j,
            best: self.best + other.best,
            clashes: {
                self.clashes.append(&mut other.clashes);
                self.clashes
            },
            teacher: {
                if self.teacher.is_some() {
                    self.teacher
                } else {
                    other.teacher
                }
            },
        }
    }
}

impl PartialOrd for ClashRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.best.partial_cmp(&other.best)
    }
}

impl Default for ClashRecord {
    fn default() -> Self {
        Self::new()
    }
}
```
