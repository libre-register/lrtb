```rust
use crate::StudentBody;
use crate::Teacher;
use std::cell::RefCell;
use std::fmt::{Display, Formatter};
use std::rc::Rc;

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd)]
pub struct Class {
    pub teacher: Option<Rc<RefCell<Teacher>>>,
    pub student_body: Rc<RefCell<StudentBody>>,
    pub slot: Option<(u64, u64)>,
}

impl Class {
    /// Creates a new class rfom a student body. If an ID is given then
    /// that will be used, howver if one is not then it will be auto
    /// generated, and will probably be insensible.
    pub fn new(student_body: &Rc<RefCell<StudentBody>>) -> Self {
        Class {
            teacher: None,
            student_body: Rc::clone(student_body),
            slot: None,
        }
    }
}

impl Display for Class {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}\n{}",
            self.student_body.borrow().qualification,
            match &self.teacher {
                None => String::new(),
                Some(teacher) => teacher.borrow().name.clone(),
            }
        )
    }
}
```
