<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [LrTB](#lrtb)
  - [Features](#features)
  - [Modules](#modules)
    - [Main](#main)
    - [Serde](#serde)
    - [Utilities](#utilities)
  - [Max recursion](#max-recursion)
  - [Imports](#imports)

<!-- END doctoc generated TOC please keep comment here to allow auto update
--> --> --> --> --> -->

# LrTB

## Features

Just set up the features we need:
 * `backtrace` - for debugging
 * `format_args_capture` - also for debugging

```rust
#![feature(format_args_capture)]
#![feature(backtrace)]
// Remove later ↑
```

## Modules

These are all the different modules in the program.

### Main

These are the main modules, this does not include the serde modules.

```rust
pub mod clash;
pub mod class;
pub mod constrain;
pub mod context;
pub mod grades;
pub mod qualification;
pub mod student;
pub mod student_body;
pub mod teacher;
pub mod timeslot;
pub mod timetable;
```

### Serde

These modules are for serde support, allowing us to serialise and
deserialise data from files.

```rust
pub mod class_raw;
pub mod context_raw;
pub mod grades_raw;
pub mod qualification_raw;
pub mod student_body_raw;
pub mod student_raw;
pub mod teacher_raw;
pub mod timeslot_raw;
pub mod timetable_raw;
```

### Utilities

These are simple utilities whish we use internally but the user should never
need to directly access.

 * Error - contains errors which may occur during the timetabling.
 * Util - Contains utilities for generating IDs and suchlike
 * StudentBodyAssorter - Creates student bodies during timetabling.

```rust
pub mod error;
pub(crate) mod student_body_assorter;
#[cfg(test)]
mod tests;
mod util;
```

## Max recursion

This is the max recursion which should never be supersceded during
timetabling. I don't know what to set it to, only that I think FET sets it
to 14.

```rust
pub const MAX_RECURSION: u8 = 13;
```

## Imports

Now we just need to import everything from all the filed for convenient
access.

```rust
// Imports
pub use class::Class;
pub use context::Context;
pub use grades::Grade;
pub use qualification::Qualification;
pub use student::Student;
pub use student_body::StudentBody;
pub use teacher::Teacher;
pub use timeslot::TimeSlot;
pub use timetable::Timetable;

pub use class_raw::ClassRaw;
pub use context_raw::ContextRaw;
pub use qualification_raw::QualificationRaw;
pub use student_body_raw::StudentBodyRaw;
pub use student_raw::StudentRaw;
pub use teacher_raw::TeacherRaw;
pub use timeslot_raw::TimeSlotRaw;
pub use timetable_raw::TimetableRaw;
```
