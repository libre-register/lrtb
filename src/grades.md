```rust
use crate::Qualification;
use std::rc::Rc;

#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash)]
/// A grade stores an individual grade in a student, so that it can be
/// put in relatively good student bodies.
pub struct Grade {
    pub qualification: Rc<Qualification>,
    /// This is a number between 0 and 1,000,000,000 (because floats are
    /// too inaccurate)
    pub grade: u32,
}

impl Grade {
    pub fn new(qualification: Rc<Qualification>, grade: u32) -> Self {
        Self {
            qualification,
            grade,
        }
    }
}
```
