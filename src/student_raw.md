```rust
use crate::grades_raw::GradeRaw;
use serde::Deserialize;
use serde::Serialize;

/// Stores an individual student.
///
/// Note this is the raw version your probably want the normal version.
#[derive(
    Eq, PartialEq, Debug, Hash, Ord, PartialOrd, Clone, Deserialize, Serialize,
)]
pub struct StudentRaw {
    pub name: String,
    pub subjects: Vec<String>,
    #[serde(default)]
    pub grades: Vec<GradeRaw>,
}

impl StudentRaw {
    pub fn new(
        name: String,
        subjects: Vec<String>,
        grades: Vec<GradeRaw>,
    ) -> Self {
        StudentRaw {
            name,
            subjects,
            grades,
        }
    }
}
```
