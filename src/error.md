<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Error](#error)
  - [Tabling Error](#tabling-error)
  - [ContextRaw to Context Errors](#contextraw-to-context-errors)
    - [Base Enum](#base-enum)
    - [Format](#format)
    - [Error](#error-1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
--> -->

# Error

These are for all the weird and disapointing errors this program can get.

First lets just set up the imports.

```rust
use crate::{Student, StudentBody};
use std::cell::RefCell;
use std::fmt::{Display, Formatter, Result};
use std::rc::Rc;
```

## Tabling Error

This error is for things which happen during the timetabling process. This
means that they are probably the hardest to deal with and need a great deal
of work before they will become usable.

```rust
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Debug)]
/// Contains Errors which may happen during timetabling.
///
/// Most errors contain both a name and a description when formatted, separated
/// by a newline.
pub enum TablingError {
    /// This signifies that a body was not found when removing from a
    /// timetable. This is something the user should not have to see and if
    /// they do should file it as an issue over at
    /// [LrTB's Gitlab](https://gitlab.com/libre-register/lrtb/-/issues)
    BodyNotFound,
    /// This signifies a class was not found when attempting its removal from a
    /// timetable. If you remove a class manually you might see this otherwise
    /// file it as an issue.
    ClassNotFound,
    /// This signifies that a student body which is being timetabled has more
    /// sessions than can be allocated. For example given a 3x3 timetable a
    /// body which has 10 session cannot be timetabled.
    TooManySessions(Rc<RefCell<Student>>),
    /// This signifies that a class has already been added and is being added
    /// again.
    ClassAlreadyAdded,
    /// This is when a student body gets excluded from every slot on the
    /// timetable
    FullyExcludedBody(Rc<RefCell<StudentBody>>),
    /// No more reverts. This is when the timetabling algorithm has run out
    /// of things to rever to and has given up.
    NoMoreReverts,
}

impl std::error::Error for TablingError {}

impl Display for TablingError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match &self {
            Self::BodyNotFound => write!(
                f,
                "Body not found.\n\nA student body was attempted to be \
    			removed, however the timeslot which contained the body does \
    			not exist."
            ),
            Self::ClassNotFound => write!(
                f,
                "Class not found\n\nA class was attempted to be removed but the \
				timeslot which was assumed to have the class did not have it."
            ),
            Self::TooManySessions(e) => write!(
                f,
                "Too many sessions\n\nThis means that a student has to attend more
				sessions than are physically possible. {}",
				e.borrow().id,
            ),
            Self::ClassAlreadyAdded => write!(
                f,
                "Class already added\n\nThis means that a class has been added and \
				another part of the program is trying to add it to another slot on \
				the timetable."
            ),
            Self::FullyExcludedBody(body) => write!(
                f,
                "Fully Excluded Body\n\nThis means that a student body has been \
				tried against every slot in the timetable with no avail. There is \
				not much you can do about this, unfortunately.\n\nThe body excluded has \
				the id of: \"{}\"",
                body.borrow().id,
            ),
            Self::NoMoreReverts => write!(
                f,
                "No More Reverts\n\nThis occurs when the timetabling algorithm \n\
        		has ran out of things to revert back to. Moreover, this likely means \n\
        		that the timetable is impossible or that it is too difficult for the \n\
        		algorithm to solve."
            ),
        }
    }
}
```

## ContextRaw to Context Errors

These are errors specific to the conversion of contexts and context raws.

### Base Enum

```rust
#[derive(Clone, Debug)]
pub enum ContextRawToContextError {
    /// This means that a key has not been found when trying to fetch it to
    /// reference an item. An example would be if you tried to access a
    /// qualification called "qualification2" but there is no qualification
    /// with that ID
    KeyNotFound(String),
}
```

### Format

All errors need to implement format.

```rust
impl Display for ContextRawToContextError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match &self {
            Self::KeyNotFound(key) => write!(f, "Key Not Found: {key}"),
        }
    }
}
```

### Error

This turns it into an actual error.

```rust
impl std::error::Error for ContextRawToContextError {}
```
