```rust
use serde::Deserialize;
use serde::Serialize;

//For TOML, and i guess JSON, support
#[derive(
    Deserialize, Serialize, Hash, Eq, PartialEq, Ord, PartialOrd, Clone, Debug,
)]
pub struct QualificationRaw {
    pub display_name: String,
    #[serde(default = "one")]
    pub optimum_class_size: usize,
    #[serde(default = "one")]
    pub sessions: usize,
}

fn one() -> usize {
    1
}

impl QualificationRaw {
    pub fn new(
        display_name: String,
        optimum_class_size: usize,
        sessions: usize,
    ) -> Self {
        Self {
            display_name,
            optimum_class_size,
            sessions,
        }
    }
}
```
