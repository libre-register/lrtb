```rust
use crate::Grade;
use crate::Qualification;
use crate::StudentBody;
use core::cell::RefCell;
use std::rc::Rc;

/// Stores an individual student.
#[derive(Eq, PartialEq, Debug, Hash, Ord, PartialOrd, Clone)]
pub struct Student {
    pub name: String,
    pub subjects_to_take: Vec<Rc<Qualification>>,
    pub grades: Vec<Grade>,
    pub id: String,
}
impl Student {
    pub fn new(name: String, subjects_to_take: Vec<Rc<Qualification>>) -> Self {
        Self {
            id: crate::util::gen_id(&name),
            name,
            subjects_to_take,
            grades: Vec::new(),
        }
    }

    pub fn new_with_id(
        name: String,
        subjects_to_take: Vec<Rc<Qualification>>,
        id: String,
    ) -> Self {
        Student {
            name,
            subjects_to_take,
            id,
            grades: Vec::new(),
        }
    }

    /// Checks which, if any subjects still need to be taken by this
    /// student.
    pub(crate) fn find_unsatisfied_subjects(
        &self,
        bodies: &[Rc<RefCell<StudentBody>>],
    ) -> Vec<Rc<Qualification>> {
        // Creates a vector containing all of the qualifications we have
        let mut vec = self.subjects_to_take.clone();

        // Begins to loop through the body
        '_body: for body in bodies {
            // Checks were in this body.
            let mut is_match = false;
            for student in &body.borrow().students {
                if student.borrow().eq(self) {
                    is_match = true;
                }
            }
            if !is_match {
                continue '_body;
            }
            // Removes any uneeded qualifications from our list
            vec.retain(|x| x != &body.borrow().qualification);
        }

        vec
    }

    /// Gets the average grade for a certain subject
    pub fn get_grade(&self, qualification: &Rc<Qualification>) -> u64 {
        let mut total = 0_u64;
        let mut count = 0_u64;
        for grade in &self.grades {
            if &grade.qualification != qualification {
                continue;
            }
            total += u64::from(grade.grade);
            count += 1;
        }
        if count == 0 {
            0_u64
        } else {
            total / count
        }
    }
}
```
