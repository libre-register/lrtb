```rust
use serde::{Deserialize, Serialize};

/// A structure to store a teacher, Raw version
#[derive(Debug, Hash, Eq, PartialEq, Clone, Deserialize, Serialize)]
pub struct TeacherRaw {
    pub name: String,
    pub qualifications: Vec<String>,
}

impl TeacherRaw {
    pub fn new(name: String, qualifications: Vec<String>) -> Self {
        Self {
            name,
            qualifications,
        }
    }
}
```
