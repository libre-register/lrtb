```rust
use crate::error::TablingError;
use crate::Class;
use crate::Student;
use crate::StudentBody;
use crate::TimeSlot;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Debug, Eq, PartialEq, Clone)]
/// Stores a finished timetable
pub struct Timetable {
    pub arr: Vec<Vec<TimeSlot>>,
}

impl From<Vec<Vec<TimeSlot>>> for Timetable {
    /// Creates a new timetable from a 2D Vector.
    fn from(value: Vec<Vec<TimeSlot>>) -> Self {
        Timetable { arr: value }
    }
}

impl Timetable {
    /// Creates a new timetable
    pub fn new(days: u64, sessions: u64) -> Self {
        // Creates the days
        let mut cdays = Vec::with_capacity(days as usize);
        // Populates it to make it a 2D Vec
        for i in 0..days {
            let mut day = Vec::with_capacity(sessions as _);
            for j in 0..sessions {
                day.push(TimeSlot::new(i, j))
            }

            cdays.push(day);
        }

        Timetable::from(cdays)
    }

    /// Removes all excluded bodies
    pub fn remove_excluded_bodies(&mut self) {
        for i in &mut self.arr {
            for j in i {
                j.remove_excluded_bodies();
            }
        }
    }

    /// This function gets how many sessions a body is excluded from.
    pub fn get_excluded_count(
        &mut self,
        body: &Rc<RefCell<StudentBody>>,
    ) -> usize {
        // Variable to store how many exclusions this body has got
        let mut exclusions = 0_usize;
        for day in &self.arr {
            for session in day {
                // Checks for a match
                if session.excluded_bodies.contains(body) {
                    exclusions += 1;
                }
            }
        }
        exclusions
    }

    /// Checks if there are any bodies which have been excluded from
    /// all slots. If so then we will return true, if not false.
    ///
    /// This is used internally to determine wether or not the
    /// timetabling should quit out.
    pub fn check_fully_excluded(
        &mut self,
        bodies: &[Rc<RefCell<StudentBody>>],
    ) -> Option<Rc<RefCell<StudentBody>>> {
        // Loop through each body
        for body in bodies {
            // Checks if we have passed peak exclusions.
            if self.get_excluded_count(body)
                >= self.arr.len() * self.arr[0].len()
            {
                return Some(Rc::clone(body));
            }
        }
        None
    }
    /// Converts this to a tableprint table, this allows you to print.
    /// This does involve clones.
    pub fn get_tableprint_tables(
        &self,
        students: &[Rc<RefCell<Student>>],
    ) -> HashMap<String, table_print::Table> {
        let mut students = students.to_owned();
        students.sort_unstable_by(|a, b| {
            a.borrow().name.partial_cmp(&b.borrow().name).unwrap()
        });

        let mut hashmap = HashMap::new();
        for student in students {
            // Create a table using the days of the week.
            let mut table = table_print::Table::new(
                vec![
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday",
                ][0..self.arr.len()]
                    .to_owned()
                    .iter()
                    .map(|x| String::from(*x))
                    .collect(),
            );

            // Now we begin to loop through us
            for day in &self.arr {
                table.content.push(
                    day.iter()
                        .map(|slot| {
                            let sessions =
                                slot.give_students_sessions(&student);

                            sessions
                                .iter()
                                .map(|x| x.borrow().to_string())
                                .collect()
                        })
                        .collect(),
                )
            }
            hashmap.insert(student.borrow().name.clone(), table);
        }
        hashmap
    }

    /// Removes a body from the timetable.
    pub fn remove_body(
        &mut self,
        body: &Rc<RefCell<StudentBody>>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        for day in &mut self.arr {
            for session in day {
                let class = session.get_body_classes(body);
                if let Some(class) = class {
                    session.remove_class(&class)?;
                }
            }
        }

        Ok(())
    }

    /// Removes a class from the timetable.
    pub fn remove_class(
        &mut self,
        class: &Rc<RefCell<Class>>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        if class.borrow().slot.is_none() {
            return Err(Box::new(TablingError::ClassNotFound));
        }
        self.arr[class.borrow().slot.unwrap().0 as usize]
            [class.borrow().slot.unwrap().1 as usize]
            .remove_class(class)?;

        Ok(())
    }

    /// Corrects the student bodies filled sessions.
    pub(crate) fn correct_filled_sessions(
        &self,
        bodies: &mut [Rc<RefCell<StudentBody>>],
    ) {
        // Loop through each body
        for body in bodies {
            // Reset their filled sessions
            body.borrow_mut().filled_sessions = 0;

            // Goes through the timetable
            for day in &self.arr {
                for session in day {
                    // Checks for a match
                    if session.get_body_classes(body).is_some() {
                        body.borrow_mut().filled_sessions += 1;
                    }
                }
            }
        }
    }

    /// Gets all the student bodies used in this timetable.
    pub fn get_bodies(&self) -> Vec<Rc<RefCell<StudentBody>>> {
        // List of bodies
        let mut bodies = Vec::new();

        for day in &self.arr {
            for session in day {
                for class in &session.classes {
                    if !bodies.contains(&class.borrow().student_body) {
                        bodies.push(Rc::clone(&class.borrow().student_body))
                    }
                }
            }
        }

        bodies
    }
}
```
