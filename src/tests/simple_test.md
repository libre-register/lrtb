```rust
use super::general::run_test;

/// Easy Test
///
/// The point of this test is to give it something so easy that if
/// it fudges it up there will be no other words but sadness.
/// Please don't fudge it up...
#[test]
fn simple_test() -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    run_test(include_str!("data/simple_test.toml"))
}

#[test]
fn graded_test_simple() -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    run_test(include_str!("data/graded_test.toml"))
}

#[test]
fn graded_test_lock() -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    run_test(include_str!("data/graded_test_lock.toml"))
}

#[test]
fn graded_test_lock_double(
) -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    run_test(include_str!("data/graded_test_lock_double.toml"))
}
```
