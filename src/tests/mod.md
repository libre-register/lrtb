<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Tests](#tests)

<!-- END doctoc generated TOC please keep comment here to allow auto update
--> --> --> --> --> -->

# Tests

This directory contains all the tests for the program.

```rust
mod easy_test;
mod general;
mod impossible_test;
mod individual_functions;
mod simple_test;
```
