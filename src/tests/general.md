```rust
use crate::Context;
use crate::ContextRaw;
use std::convert::TryInto;
use std::error::Error;
use std::fs;
use std::io::Write;

const PATH: &'static str = "src/tests/results/test.txt";

pub fn run_test(toml: &str) -> Result<(), Box<dyn Error>> {
    // Remove any old test files
    fs::File::create(PATH)?;

    // Creates the context
    let contextr: ContextRaw = toml::from_str(toml)?;

    let mut context: Context = contextr.try_into()?;

    context.gen(Some(&callback))?;

    Ok(())
}

// Callback function
fn callback(context: &Context, percent: f64) {
    // Prints the percentage, not important now but maybe in the future?
    println!("Percentage_Complete: {}", percent);
    // Writes the timetable to a file.
    let tables = context
        .timetable
        .as_ref()
        .unwrap()
        .get_tableprint_tables(context.students.as_slice());

    // Open the file
    let mut file = fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open(PATH)
        .expect("error_opening_file");

    let mut content = String::new();

    for (name, table) in tables {
        // Write the name
        content.push_str("\n====[ ");
        content.push_str(&name);
        content.push_str(" ]=====\n\n");

        // Write the table
        content.push_str(&table.get_pretty(80).unwrap());
    }

    content.push_str(
        "\n \
    	||-------------------------------------|| \n \
    	||=O===O===O===O===O===O===O===O===O===|| \n \
    	||-------------------------------------|| \n \
    	",
    );

    file.write_all(content.as_bytes())
        .expect("WRITING TO FILE FAILED");
}
```
