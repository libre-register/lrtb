<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Generalised imports](#generalised-imports)
- [Deserialisation](#deserialisation)
- [Timetable Tests](#timetable-tests)
  - [New function](#new-function)
  - [Remove excluded bodies.](#remove-excluded-bodies)
  - [Fully Excluded Bodies](#fully-excluded-bodies)
  - [Timetable Bodies Test](#timetable-bodies-test)
  - [Timetable Remove Bodies Test](#timetable-remove-bodies-test)
- [Timeslot Tests](#timeslot-tests)
  - [New Function](#new-function)
  - [Setup](#setup)
  - [Remove excluded bodies](#remove-excluded-bodies)
  - [Add Classes](#add-classes)
  - [Give student sessions](#give-student-sessions)
  - [Remove Class](#remove-class)
- [Student](#student)
  - [New Functions](#new-functions)
    - [Simple New](#simple-new)
    - [New with ID](#new-with-id)
  - [Find Unsatisfied Subjects](#find-unsatisfied-subjects)
  - [Get grade](#get-grade)
- [Student Body](#student-body)
  - [New Function](#new-function-1)
  - [Check Clashes](#check-clashes)
    - [Setup](#setup-1)
    - [Individual Class Clashes](#individual-class-clashes)
      - [No Teachers](#no-teachers)
      - [With Teachers](#with-teachers)
    - [Individual Timeslot Clashes](#individual-timeslot-clashes)
      - [Without Teachers](#without-teachers)
      - [With Teachers](#with-teachers-1)
    - [Get Least Clashes](#get-least-clashes)
    - [Finishing Brace](#finishing-brace)

<!-- END doctoc generated TOC please keep comment here to allow auto update
--> -->

This file has many smaller tests for individual parts of the program,
rather than the program as a whole.

## Generalised imports

This just imports everything we will need.

```rust
use crate::clash::*;
use crate::*;
use common_macros::*;
use std::{cell::RefCell, collections::HashMap, convert::TryInto, rc::Rc};
```

## Deserialisation

This checks that we can deserialise from a file.

```rust
#[test]
fn deserialise() {
    // Create the context, raw version
    let context_toml: ContextRaw =
        toml::from_str(include_str!("individual_functions.toml")).unwrap();

    // Creates a context, normal version
    let mut context: Context = context_toml.try_into().unwrap();

    // Expected qualifications, what we assume we will get from the
    // serialisation.
    let qualifications: Vec<Rc<Qualification>> = vec![
        Qualification {
            id: String::from("maths"),
            display_name: Some(String::from("Maths")),
            optimum_class_size: 2,
            sessions: 1,
        },
        Qualification {
            id: String::from("science"),
            display_name: Some(String::from("Science")),
            optimum_class_size: 2,
            sessions: 1,
        },
        Qualification {
            id: String::from("english"),
            display_name: Some(String::from("English")),
            optimum_class_size: 2,
            sessions: 1,
        },
    ]
    .into_iter()
    .map(Rc::new)
    .collect();
    // Checks they are all in each other.
    for qualification in &qualifications {
        assert!(context.qualifications.contains(qualification));
    }

    assert_eq!(context.days, 3);
    assert_eq!(context.sessions, 1);

    context.students.sort();

    let mut students = vec![
        Student {
            id: String::from("john_doe"),
            name: String::from("John Doe"),
            grades: vec![
                Grade::new(Rc::clone(&qualifications[0]), 500_000_000),
                Grade::new(Rc::clone(&qualifications[0]), 750_000_000),
                Grade::new(Rc::clone(&qualifications[1]), 1_000_000_000),
                Grade::new(Rc::clone(&qualifications[2]), 100_000_000),
            ],
            subjects_to_take: vec![
                &qualifications[0],
                &qualifications[1],
                &qualifications[2],
            ]
            .into_iter()
            .map(Rc::clone)
            .collect(),
        },
        Student {
            id: String::from("anne_parker"),
            name: String::from("Anne Parker"),
            grades: vec![
                Grade::new(Rc::clone(&qualifications[0]), 500_000_000),
                Grade::new(Rc::clone(&qualifications[0]), 750_000_000),
                Grade::new(Rc::clone(&qualifications[2]), 1_000_000_000),
                Grade::new(Rc::clone(&qualifications[1]), 100_000_000),
            ],
            subjects_to_take: vec![
                &qualifications[0],
                &qualifications[1],
                &qualifications[2],
            ]
            .into_iter()
            .map(Rc::clone)
            .collect(),
        },
        Student {
            id: String::from("charlie_chaplin"),
            name: String::from("Charlie Chaplin"),
            grades: vec![
                Grade::new(Rc::clone(&qualifications[0]), 100_000_000),
                Grade::new(Rc::clone(&qualifications[2]), 500_000_000),
                Grade::new(Rc::clone(&qualifications[1]), 100_000_000),
            ],
            subjects_to_take: vec![
                &qualifications[0],
                &qualifications[1],
                &qualifications[2],
            ]
            .into_iter()
            .map(Rc::clone)
            .collect(),
        },
        Student {
            id: String::from("lilly_hopper"),
            name: String::from("Lilly Hopper"),
            grades: vec![
                Grade::new(Rc::clone(&qualifications[0]), 1_000_000_000),
                Grade::new(Rc::clone(&qualifications[2]), 700_000_000),
                Grade::new(Rc::clone(&qualifications[1]), 200_000_000),
            ],
            subjects_to_take: vec![
                &qualifications[0],
                &qualifications[1],
                &qualifications[2],
            ]
            .into_iter()
            .map(Rc::clone)
            .collect(),
        },
    ]
    .into_iter()
    .map(RefCell::new)
    .map(Rc::new)
    .collect::<Vec<Rc<RefCell<Student>>>>();
    students.sort();

    assert_eq!(context.students, students);
    assert_eq!(context.timetable, None);
    assert_eq!(context.student_bodies, Vec::new());

    let mut teachers = vec![
        Teacher {
            id: String::from("mr_maths2"),
            name: String::from("Mr Maths 2"),
            qualifications: vec![qualifications[0].clone()],
        },
        Teacher {
            id: String::from("mr_maths"),
            name: String::from("Mr Maths"),
            qualifications: vec![qualifications[0].clone()],
        },
        Teacher {
            id: String::from("mr_science2"),
            name: String::from("Mr Science 2"),
            qualifications: vec![qualifications[1].clone()],
        },
        Teacher {
            id: String::from("mr_science"),
            name: String::from("Mr Science"),
            qualifications: vec![qualifications[1].clone()],
        },
        Teacher {
            id: String::from("mr_english"),
            name: String::from("Mr English"),
            qualifications: vec![qualifications[2].clone()],
        },
    ]
    .into_iter()
    .map(RefCell::new)
    .map(Rc::new)
    .collect::<Vec<Rc<RefCell<Teacher>>>>();

    context.teachers.sort();
    teachers.sort();

    assert_eq!(context.teachers, teachers);
}
```

## Timetable Tests

These are tests which directly use [The Timetable](../timetable.md).

### New function

This checks that the new function creates the correct timetable.

```rust
#[test]
fn timetable_new_test() {
    assert_eq!(
        Timetable::new(3, 2),
        Timetable {
            arr: vec![
                [TimeSlot::new(0, 0), TimeSlot::new(0, 1)],
                [TimeSlot::new(1, 0), TimeSlot::new(1, 1)],
                [TimeSlot::new(2, 0), TimeSlot::new(2, 1)]
            ]
            .iter()
            .map(|x| x.to_vec())
            .collect()
        }
    );
}
```

### Remove excluded bodies.

This checks that bodies, which have been excluded, can be bulk-removed.

```rust
#[test]
fn timetable_remove_excluded_bodies() {
    // Get the raw context.
    let context_raw: ContextRaw = toml::from_str(include_str!(
        "data/individual_functions_timetable.toml"
    ))
    .unwrap();

    // Get the normal context.
    let mut context: Context = context_raw.try_into().unwrap();
    let timetable = context.timetable.as_mut().unwrap();

    // Adds some excluded bodies to the timetable.
    timetable.arr[1][1]
        .excluded_bodies
        .push(Rc::clone(&context.student_bodies[0]));
    timetable.arr[2][0]
        .excluded_bodies
        .push(Rc::clone(&context.student_bodies[0]));

    // Remove these excluded bodies.
    timetable.remove_excluded_bodies();

    // Check they have been removed.
    for day in &timetable.arr {
        for session in day {
            assert_eq!(session.excluded_bodies.len(), 0);
        }
    }
}
```

### Fully Excluded Bodies

This checks that if a body is excluded in every timeslot than the program
should crash.

```rust
#[test]
fn timetable_fully_excluded_bodies() {
    // Create a context.
    let mut context = Context::new(3, 3);

    // Adds a qualification
    context
        .qualifications
        .push(Qualification::new("q".into(), Some(String::from("Q"))).into());

    // And a student body
    context
        .student_bodies
        .push(Rc::new(RefCell::new(StudentBody::new(
            Rc::clone(&context.qualifications[0]),
            1,
            String::from("s"),
        ))));

    context.timetable = Some(Timetable::new(3, 3));
    let timetable = context.timetable.as_mut().unwrap();

    // Exclude the body.
    for day in &mut timetable.arr {
        for session in day {
            session
                .excluded_bodies
                .push(Rc::clone(&context.student_bodies[0]));
        }
    }

    assert_eq!(
        timetable.check_fully_excluded(&context.student_bodies),
        Some(Rc::clone(&context.student_bodies[0]))
    );
}
```

### Timetable Bodies Test

This test simply tests the `Timetable.get_bodies` function, to check all
bodies are being got from the timetable correctly.

Here is the toml file

```rust
#[test]
fn timetable_bodies_test() {
    let context_raw: ContextRaw = toml::from_str(include_str!(
        "data/individual_functions_timetable.toml"
    ))
    .unwrap();
```

Now we just check that the timetables are equal. If they are not then we
know that it has gone wrong.

```rust
    let mut context: Context = context_raw.try_into().unwrap();
    context
        .student_bodies
        .sort_unstable_by(|a, b| a.borrow().id.cmp(&b.borrow().id));

    assert_eq!(
        context.timetable.unwrap().get_bodies(),
        context.student_bodies[0..7]
    );
}
```

### Timetable Remove Bodies Test

This test will check for removing a whole body from a timetable. We will
also look at removing individual classes.

```rust
#[test]
fn timetable_remove_bodies_and_classestest() {
    let context_raw: ContextRaw = toml::from_str(include_str!(
        "data/individual_functions_timetable.toml"
    ))
    .unwrap();

    let mut context: Context = context_raw.try_into().unwrap();
    context
        .student_bodies
        .sort_unstable_by(|a, b| a.borrow().id.cmp(&b.borrow().id));
    let timetable = context.timetable.as_mut().unwrap();

    // Remove two bodies
    for body in context.student_bodies[5..9].iter() {
        timetable.remove_body(body).unwrap();
    }

    // Checks they have been removed.
    assert_eq!(timetable.get_bodies(), context.student_bodies[0..5]);

    // Remove a class.
    let class = Rc::clone(&timetable.arr[0][0].classes[0]);
    timetable.arr[0][0].remove_class(&class).unwrap();

    // Check its been removed.
    assert!(timetable.arr[0][0].classes.is_empty());

    // Try and remove it again
    assert_eq!(
        timetable.arr[0][0].remove_class(&class),
        Err(crate::error::TablingError::ClassNotFound)
    );
}
```

## Timeslot Tests

These tests all directly interface with the timeslot struct. Most of these
tests link back to those in timetables.

### New Function

This just tests the ability to create a new timeslot.

```rust
#[test]
fn timeslot_new() {
    assert_eq!(
        TimeSlot {
            classes: Vec::new(),
            excluded_bodies: Vec::new(),
            day: 2,
            session: 3
        },
        TimeSlot::new(2, 3)
    );
}
```

### Setup

For the following tests we will be using the same context, here is the
setup.

```rust
#[test]
fn timeslot_check() {
    // Create a context.
    let mut context = Context::new(0, 0);

    // Give it a qualification.
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("q"),
        Some(String::from("Q")),
    )));

    // Add a couple of students.
    for i in 0..2 {
        context.students.push(Rc::new(RefCell::new(Student::new(
            String::from(format!("Student-{i}")),
            vec![Rc::clone(&context.qualifications[0])],
        ))));
    }

    // Give it 2 student bodies.
    for i in 0..4 {
        context.student_bodies.push(Rc::new(RefCell::new({
            let mut body = StudentBody::new(
                Rc::clone(&context.qualifications[0]),
                1,
                String::from(format!("Body-{i}")),
            );

            body.students.push(Rc::clone(&context.students[i % 2]));

            body
        })));
    }

    // Creates a timeslot to add bodies to.
    let mut timeslot = TimeSlot::new(0, 0);
```

### Remove excluded bodies

Checks that we can remove excluded bodies from timetables. This is here
mainly for completions sake since it should already be covered in the
simmilar test for timetables.

```rust
    // Add the bodies
    timeslot
        .excluded_bodies
        .append(&mut context.student_bodies.clone());

    // Check they have been added
    assert_eq!(timeslot.excluded_bodies.len(), 4);

    // Remove them again
    timeslot.remove_excluded_bodies();

    // Check they have been removed.
    assert!(timeslot.excluded_bodies.is_empty());
```

### Add Classes

This checks that we can add classes.

```rust
    // Add the classes
    for body in &context.student_bodies {
        timeslot
            .add_class(&Rc::new(RefCell::new(Class::new(body))))
            .unwrap();
    }

    // Checks they have been added
    assert_eq!(timeslot.classes.len(), 4);
```

### Give student sessions

This checks that the timeslots can get the classes which a student has
within the timeslot.

```rust
    // Get the values
    let student_classes = timeslot.give_students_sessions(&context.students[0]);

    assert_eq!(student_classes.len(), 2);
    assert_eq!(
        student_classes,
        [
            Rc::clone(&timeslot.classes[0]),
            Rc::clone(&timeslot.classes[2])
        ]
    );
```

### Remove Class

This checks we can remove classes from the timeslot.

```rust
    // Gets the expected classes.
    let expected = timeslot.classes[1..=2].to_vec();

    // Remove the classes.
    timeslot.remove_class(&timeslot.classes[0].clone()).unwrap();
    timeslot.remove_class(&timeslot.classes[2].clone()).unwrap();

    // Check they have matched.
    assert_eq!(timeslot.classes, expected);

    // Checks what happens if we remove a class which has already been removed.
    timeslot.remove_class(&expected[0]).unwrap();
    assert_eq!(
        timeslot.remove_class(&expected[0]).unwrap_err(),
        crate::error::TablingError::ClassNotFound
    );
}
```

## Student

These will test upon students.

### New Functions

These will test creating new students.

#### Simple New

This is to check for creating a new student without a specified ID.

```rust
#[test]
fn student_new() {
    // Creates a context.
    let mut context = Context::new(1, 1);

    // Create a qualification.
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("q"),
        Some(String::from("Q")),
    )));

    let student = Student::new(
        String::from("John Doe"),
        vec![Rc::clone(&context.qualifications[0])],
    );

    // Checks it has the correct name.
    assert_eq!(student.name, "John Doe");

    // Check it has the right ID.
    assert_eq!(&student.id[0..(student.id.len() - 4)], "john_doe-");

    // Check the grades are nothing.
    assert_eq!(student.grades, vec![]);

    // Check the subjects to take are correct.
    assert_eq!(
        student.subjects_to_take,
        vec![Rc::clone(&context.qualifications[0])]
    );
}
```

#### New with ID

This checks that students can be made with a specific ID.

```rust
#[test]
fn student_new_with_id() {
    // Creates a context.
    let mut context = Context::new(1, 1);

    // Create a qualification.
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("q"),
        Some(String::from("Q")),
    )));

    let student = Student::new_with_id(
        String::from("John Doe"),
        vec![Rc::clone(&context.qualifications[0])],
        String::from("john_doe_is_cool"),
    );

    // Checks it has the correct name.
    assert_eq!(student.name, "John Doe");

    // Check it has the right ID.
    assert_eq!(&student.id, "john_doe_is_cool");

    // Check the grades are nothing.
    assert_eq!(student.grades, vec![]);

    // Check the subjects to take are correct.
    assert_eq!(
        student.subjects_to_take,
        vec![Rc::clone(&context.qualifications[0])]
    );
}
```

### Find Unsatisfied Subjects

During the generation process it is often needed to find which subjects a
student needs still to take. This is a check for it.

```rust
#[test]
fn student_find_unsatisfied_subjects() {
    // Get the raw context.
    let context_raw: ContextRaw = toml::from_str(include_str!(
        "data/individual_functions_timetable.toml"
    ))
    .unwrap();

    // Get the normal context.
    let mut context: Context = context_raw.try_into().unwrap();

    let unsatisfied_subjects = context.students[0]
        .borrow()
        .find_unsatisfied_subjects(&context.student_bodies);

    context.qualifications.sort();

    assert_eq!(
        unsatisfied_subjects,
        vec![context.qualifications.last().unwrap().clone()]
    );
}
```

### Get grade

This checks getting the grade from a student, so that they can be streamed
into classes.

```rust
#[test]
fn student_get_grade() {
    // Create a context
    let mut context = Context::new(1, 1);

    // Add a Qualification
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("q"),
        Some(String::from("Q")),
    )));
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("m"),
        Some(String::from("P")),
    )));

    // Creates a student.
    context.students.push(Rc::new(RefCell::new(Student::new(
        String::from("John Doe"),
        context.qualifications.clone(),
    ))));

    // Creates some grades to the student.
    let grades = vec![
        ("q", 500_000_000),
        ("q", 100_000_000),
        ("q", 1_000_000_000),
        ("m", 0),
        ("m", 5_000_000),
    ];

    // Adds them.
    for (name, grade) in grades {
        context.students[0].borrow_mut().grades.push(Grade::new(
            {
                context
                    .qualifications
                    .sort_unstable_by_key(|x| x.id == name);
                Rc::clone(&context.qualifications[0])
            },
            grade,
        ));
    }

    context.qualifications.sort();

    // Checks the grades
    assert_eq!(
        context.students[0]
            .borrow()
            .get_grade(&context.qualifications[0]),
        533_333_333
    );
    assert_eq!(
        context.students[0]
            .borrow()
            .get_grade(&context.qualifications[1]),
        2_500_000
    );
}
```

## Student Body

These tests work with parts of the student body. These target critical parts
of the algorithm where best classes are found.

### New Function

```rust
#[test]
fn student_body_new() {
    // Create a context
    let mut context = Context::new(1, 3);

    // Adds a qualification.
    context.qualifications.push(Rc::new(Qualification::new(
        String::from("q"),
        Some(String::from("Q")),
    )));

    // Creates our student body.
    let student_body = StudentBody::new(
        Rc::clone(&context.qualifications[0]),
        1,
        String::from("body"),
    );

    // Checks its created the right body.
    assert_eq!(
        student_body,
        StudentBody {
            qualification: Rc::clone(&context.qualifications[0]),
            students: Vec::new(),
            sessions: 1,
            filled_sessions: 0,
            teachers: Vec::new(),
            id: String::from("body"),
            difference: 0,
            generated: false,
        }
    );

    // Adds it to the context
    context
        .student_bodies
        .push(Rc::new(RefCell::new(student_body)));
}
```

### Check Clashes

Clashes are a fundemental part of the program, they are for checking that
two classes don't overlap on teachers _or_ students. Here we will do a
couple of examples, within the same function.

#### Setup

This is just some basic setup, creating the bodies and suchlike

```rust
#[test]
fn student_body_check_clashes() {
    // Deserialise from the file
    let context_raw: ContextRaw = toml::from_str(include_str!(
        "data/individual_functions_timetable.toml"
    ))
    .unwrap();

    // Get a real context.
    let mut context: Context = context_raw.try_into().unwrap();

    // Make sure its bodies are in a definite order.
    context
        .student_bodies
        .sort_unstable_by(|a, b| a.borrow().id.cmp(&b.borrow().id));

    // Same for its students
    context
        .students
        .sort_unstable_by(|a, b| a.borrow().id.cmp(&b.borrow().id));

    context
        .teachers
        .sort_unstable_by(|a, b| a.borrow().id.cmp(&b.borrow().id));

    // Get its timetable
    let timetable = context.timetable.as_mut().unwrap();

    // Creates a student body which hasn't been timetabled and can go in a
    // specific spot without any clashes.
    let mut new_body = StudentBody::new(
        Rc::clone(&context.qualifications[0]),
        3,
        String::from("body"),
    );

    // Add some students to the newly created body.
    new_body
        .students
        .append(&mut context.students[1..=2].to_vec());

    // Check we've added the right students.
    assert_eq!(new_body.students[0].borrow().id, "student2");
    assert_eq!(new_body.students[1].borrow().id, "student3");
```

#### Individual Timeslot Clashes

Of course, looking at individual classes can only get so far. We still
need to _'zoom out'_ and look at the setting as whole.

##### Without Teachers

First we will try it without teachers.

```rust
    // This should have a result of 0
    assert_eq!(
        new_body.check_clashes_timeslot(&timetable.arr[0][2], &[]),
        ClashRecord {
            i: 0,
            j: 2,
            best: 0.0,
            clashes: Vec::new(),
            teacher: None,
        },
    );

    // This should have a result of 1
    assert_eq!(
        new_body.check_clashes_timeslot(&timetable.arr[0][1], &[]),
        ClashRecord {
            i: 0,
            j: 1,
            best: 1.0,
            clashes: vec![Rc::clone(&timetable.arr[0][1].classes[0])],
            teacher: None,
        },
    );

    // This should have a result of 2
    assert_eq!(
        new_body.check_clashes_timeslot(&timetable.arr[1][0], &[]),
        ClashRecord {
            i: 1,
            j: 0,
            best: 2.0,
            clashes: vec![Rc::clone(&timetable.arr[1][0].classes[0])],
            teacher: None,
        },
    );
```

##### With Teachers

Same deal as before.

First we need to add the teachers, although this time we also have to
add them to ourselves.

```rust
    // Add teachers to the previously tested classes.
    timetable.arr[0][1].classes[0].borrow_mut().teacher =
        Some(Rc::clone(&context.teachers[0]));
    timetable.arr[1][0].classes[0].borrow_mut().teacher =
        Some(Rc::clone(&context.teachers[1]));
    new_body.teachers = context.teachers.clone();
```

Then do the tests again.

```rust
    // This should have a result of 1.5
    assert_eq!(
        new_body.check_clashes_timeslot(&timetable.arr[0][1], &[]),
        ClashRecord {
            i: 0,
            j: 1,
            best: 1.0,
            clashes: vec![Rc::clone(&timetable.arr[0][1].classes[0])],
            teacher: Some(Rc::clone(&context.teachers[0])),
        },
    );

    // This should have a result of 2.5
    assert_eq!(
        new_body.check_clashes_timeslot(&timetable.arr[1][0], &[]),
        ClashRecord {
            i: 1,
            j: 0,
            best: 2.0,
            clashes: vec![Rc::clone(&timetable.arr[1][0].classes[0])],
            teacher: Some(Rc::clone(&context.teachers[0])),
        },
    );
```

#### Get Least Clashes

Both functions above only (really) serve in order for this function to
work. This function simply looks at the timetable, and finds the best
possible slot to go in.

```rust
    // Get the best slot
    let slot = new_body.get_least_clashes(&context.timetable.as_ref().unwrap());

    // Checks it
    assert!(
        slot == ClashRecord {
            i: 1,
            j: 1,
            best: 0.0,
            clashes: Vec::new(),
            teacher: Some(Rc::clone(&new_body.teachers[0])),
        } || slot
            == ClashRecord {
                i: 0,
                j: 2,
                best: 0.0,
                clashes: Vec::new(),
                teacher: Some(Rc::clone(&new_body.teachers[0])),
            }
    )
```

#### Finishing Brace
```rust
}
```
