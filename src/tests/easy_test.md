```rust
use super::general::run_test;

/// Easy Test
///
/// The point of this test is to give it something so easy that if
/// it fudges it up there will be no other words but sadness.
/// Please don't fudge it up...
#[test]
fn easy_test() -> Result<(), Box<(dyn std::error::Error + 'static)>> {
    run_test(include_str!("data/easy_test.toml"))
}
```
