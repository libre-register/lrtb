```rust
use super::general::run_test;

/// Toml Test Impossible
///
/// This test aims to give the algorithm an impossible puzzle, and to see
/// what happens.
#[test]
fn impossible_toml_test() {
    run_test(include_str!("data/impossible_test.toml"))
        .err()
        .unwrap(); // Should be an error for this test
}

#[test]
fn impossible_toml_test2() {
    run_test(include_str!("data/impossible_test2.toml"))
        .err()
        .unwrap(); // Should be an error for this test
}
```
