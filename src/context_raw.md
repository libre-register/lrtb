```rust
use crate::grades_raw::GradeRaw;
use crate::ClassRaw;
use crate::Context;
use crate::QualificationRaw;
use crate::StudentBodyRaw;
use crate::StudentRaw;
use crate::TeacherRaw;
use crate::TimeSlotRaw;
use crate::TimetableRaw;
use core::convert::TryFrom;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Deserialize, Serialize)]
/// A raw version of context, can be used with toml and later converted
/// into a context.
///
/// It contains no logic other than that for conversion.
pub struct ContextRaw {
    #[serde(default)]
    pub teachers: HashMap<String, TeacherRaw>,
    pub days: u64,
    pub sessions: u64,
    #[serde(default)]
    pub student_bodies: HashMap<String, StudentBodyRaw>,
    pub students: HashMap<String, StudentRaw>,
    pub timetable: Option<TimetableRaw>,
    pub qualifications: HashMap<String, QualificationRaw>,
}

impl ContextRaw {
    /// Creates a new Context
    pub fn new(days: u64, sessions: u64) -> Self {
        ContextRaw {
            teachers: HashMap::new(),
            days,
            sessions,
            student_bodies: HashMap::new(),
            students: HashMap::new(),
            timetable: None,
            qualifications: HashMap::new(),
        }
    }
}

// For conversion
impl TryFrom<Context> for ContextRaw {
    type Error = &'static str;

    // The Conversion Function.
    fn try_from(mut context: Context) -> Result<ContextRaw, Self::Error> {
        // First the base self.
        let mut nself = Self::new(context.days, context.sessions);

        // Timetables
        //
        // First we have to check if there even is a timetable, and
        // wether we have to waste out time trying to parse it.
        nself.timetable = match context.timetable {
            // If its None we don't have to worry about anything.
            // This makes me happy.
            None => None,
            // Otherwise we are going to have to turn it into a
            // `TimetableRaw`
            Some(timetable) => {
                // Using from here so that we can just use a plain old
                // `Vec`.
                Some(TimetableRaw::from({
                    // Array to store our nonsense in
                    let mut arr = Vec::with_capacity(timetable.arr.len());

                    for day in timetable.arr {
                        // Creates the vector for this day
                        let mut day_arr = Vec::with_capacity(day.len());

                        // Loops through today's sessions
                        for mut session in day {
                            day_arr.push({
                                TimeSlotRaw::new({
                                    // Creates the new vec
                                    let mut classes = Vec::with_capacity(
                                        session.classes.len(),
                                    );
                                    // Loops through all the classes
                                    for class in session.classes.drain(..) {
                                        // Creates and pushes the new class
                                        classes.push(ClassRaw::new(
                                            class
                                                .borrow()
                                                .teacher
                                                .as_ref()
                                                .map(|teacher| {
                                                    teacher.borrow().id.clone()
                                                }),
                                            class
                                                .borrow()
                                                .student_body
                                                .borrow()
                                                .id
                                                .clone(),
                                        ));
                                    }
                                    // Returns the new classes
                                    classes
                                })
                            })
                        }
                        // Adds it back onto the bigger list
                        arr.push(day_arr);
                    }

                    // Return it
                    arr
                }))
            }
        };

        // Student Bodies
        while let Some(body) = context.student_bodies.pop() {
            let body = Rc::try_unwrap(body);
            // Tries to own the value. If not then we have to return out
            // of the function.
            // TODO: Create a codes.md so that we can not what codes mean,
            // TODO: so that they can be sent over the network and clients
            // TODO: can interperate what has happened.
            // TODO:
            // TODO: Would also be good for debugging.
            let body = match body {
                Ok(body) => body.into_inner(),
                Err(_) => return Err("class_already_borrowed"),
            };

            // Move it into the new body.
            nself.student_bodies.insert(
                body.id,
                StudentBodyRaw::new(
                    body.qualification.id.clone(),
                    {
                        let mut students = Vec::new();
                        for student in body.students {
                            // TODO: See if I can remove this clone.
                            // TODO: Clones just annoy me for no
                            // TODO: Reason.
                            students.push(student.borrow().id.clone())
                        }
                        students
                    },
                    body.sessions,
                    body.teachers
                        .iter()
                        .map(|x| x.borrow().id.clone())
                        .collect(),
                ),
            );
        }

        // Next the students
        while let Some(student) = context.students.pop() {
            let student = Rc::try_unwrap(student);
            // Gets the student, note how it may return an error if
            // already borrowed. I really hate Rc, but they are much
            // better than the impossible to wrangle lifetimes.
            let mut student = match student {
                Ok(student) => student.into_inner(),
                Err(_) => return Err("student_already_borrowed"),
            };

            // Move it into the new student, without any clones.
            // Of course this relies on the aformentioned value not being
            // borrowed.
            //
            // TODO: Create a non errorneous yet cloning version of
            // TODO: this function.
            //
            // Adds this to th
            nself.students.insert(
                student.id,
                StudentRaw::new(
                    student.name,
                    // Creates the new subjects to take from the
                    // qualification.
                    {
                        // Creates the vec
                        let mut vec = Vec::new();
                        // Drains the subjects to take
                        for qualification in student.subjects_to_take.drain(..)
                        {
                            vec.push(qualification.id.clone());
                        }
                        // Returns it
                        vec
                    },
                    student
                        .grades
                        .into_iter()
                        .map(|x| {
                            GradeRaw::new(x.qualification.id.clone(), x.grade)
                        })
                        .collect(),
                ),
            );
        }

        // Teachers
        while let Some(teacher) = context.teachers.pop() {
            let teacher = Rc::try_unwrap(teacher);
            // Gets the teacher, note how it may return an error if
            // already borrowed. I really hate Rc, but they are much
            // better than the impossible to wrangle lifetimes.
            let mut teacher = match teacher {
                Ok(teacher) => teacher.into_inner(),
                Err(_) => return Err("teacher_already_borrowed"),
            };

            // Move it into the new teacher, without any clones.
            // Of course this relies on the aformentioned value not being
            // borrowed.
            //
            // TODO: Create a non errorneous yet cloning version of
            // TODO: this function.
            //
            // Adds this to th
            nself.teachers.insert(
                teacher.id,
                TeacherRaw::new(
                    teacher.name,
                    // Creates the new subjects to take from the
                    // qualification.
                    {
                        // Creates the vec
                        let mut vec = Vec::new();
                        // Drains the subjects to take
                        for qualification in teacher.qualifications.drain(..) {
                            vec.push(qualification.id.clone());
                        }
                        // Returns it
                        vec
                    },
                ),
            );
        }

        while let Some(qualification) = context.qualifications.pop() {
            let result_qualification = Rc::try_unwrap(qualification);

            let qualification;

            match result_qualification {
                Ok(result_qualification) => {
                    qualification = result_qualification
                }
                Err(_) => return Err("qualification_already_in_use"),
            }

            nself.qualifications.insert(
                qualification.id,
                QualificationRaw::new(
                    qualification.display_name.unwrap(),
                    qualification.optimum_class_size,
                    qualification.sessions,
                ),
            );
        }

        // Return the new context after this whole ordeal.
        Ok(nself)
    }
}
```
