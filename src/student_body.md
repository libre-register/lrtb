```rust
use crate::clash::*;
use crate::constrain::{self, Constraint};
use crate::Class;
use crate::Qualification;
use crate::Student;
use crate::Teacher;
use crate::TimeSlot;
use crate::Timetable;
use std::cell::RefCell;
use std::rc::Rc;

/// Stores a body of students, example a class.
#[derive(Eq, PartialEq, Debug, Ord, PartialOrd, Clone)]
pub struct StudentBody {
    pub qualification: Rc<Qualification>,
    pub students: Vec<Rc<RefCell<Student>>>,
    pub sessions: i32,
    pub filled_sessions: i32,
    pub teachers: Vec<Rc<RefCell<Teacher>>>,
    pub id: String,
    pub(crate) difference: i32,
    pub(crate) generated: bool,
}

impl StudentBody {
    pub fn new(
        qualification: Rc<Qualification>,
        sessions: i32,
        id: String,
    ) -> Self {
        StudentBody {
            qualification,
            students: Vec::new(),
            sessions,
            filled_sessions: 0,
            teachers: Vec::new(),
            id,
            generated: false,
            difference: 0,
        }
    }

    /// Checks the clashes with a timeslot
    ///
    /// Here Excluded refers to classes not to check
    pub fn check_clashes_timeslot(
        &self,
        timeslot: &TimeSlot,
        excluded: &[Rc<RefCell<Class>>],
    ) -> ClashRecord {
        // Creates the clash record
        let mut record = ClashRecord::new();
        record.i = timeslot.day;
        record.j = timeslot.session;
        record.best = 0.0;

        let mut teachers = self.teachers.clone();

        // Loops through all the classes in the timeslot.
        for class in &timeslot.classes {
            // Remove the uneeded teachers
            teachers.retain(|x| Some(x) != class.borrow().teacher.as_ref());
        }
        record.teacher = teachers.get(0).map(Rc::clone).and(self.teachers.get(0).map(Rc::clone));
        let mut constraints: [Box<dyn Constraint>; 2] = [
            Box::new(constrain::TeacherConstraint::default()),
            Box::new(constrain::StudentConstraint::default()),
        ];
        for constraint in &mut constraints {
            let checked = constraint.check(self, timeslot, excluded);
            record.best += checked.0;
            record.clashes.extend(checked.1);
        }

        record.clashes.sort();
        record.clashes.dedup();

        record
    }

    /// Gets the least clashing timeslot
    pub(crate) fn get_least_clashes(
        &self,
        timetable: &Timetable,
    ) -> ClashRecord {
        let mut clash_record = ClashRecord::new();
        // Begins to loop through
        '_loop1: for (i, a1) in timetable.arr.iter().enumerate() {
            '_slots: for (j, slot) in a1.iter().enumerate() {
                // No Clashes at all
                if slot.classes.len() as u64 == 0 {
                    clash_record = ClashRecord::new();
                    clash_record.i = i as u64;
                    clash_record.j = j as u64;
                    clash_record.best = 0.0;
                    clash_record.teacher = self.teachers.get(0).map(Rc::clone);
                }

                // Excluded
                for student_body in &slot.excluded_bodies {
                    if student_body.borrow().eq(self) {
                        continue '_slots;
                    }
                }

                let clashes = self.check_clashes_timeslot(slot, &[]);
                if clashes < clash_record {
                    clash_record = clashes;
                }

                // No clashes
                if clash_record.best == 0.0 {
                    break '_loop1;
                }
            }
        }
        // If all else fails
        clash_record
    }
}
```
