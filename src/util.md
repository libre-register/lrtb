<!-- START doctoc generated TOC please keep comment here to allow auto
update --> <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Gen ID](#gen-id)

<!-- END doctoc generated TOC please keep comment here to allow auto update
--> -->

# Gen ID

This generates an ID based of a name.

```rust
use rand::Rng;

/// Autogenerates an ID using the name and turning it
/// into snake case. Then adds 4 hex digits to the end
pub(crate) fn gen_id(name: &str) -> String {
    format!(
        "{}-{:04x}",
        name.trim().to_lowercase().replace(" ", "_"),
        rand::thread_rng().gen::<u16>()
    )
}
```
