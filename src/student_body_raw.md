```rust
use serde::Deserialize;
use serde::Serialize;
// For TOML, and i guess JSON, support
#[derive(Deserialize, Serialize)]
pub struct StudentBodyRaw {
    pub qualification: String,
    pub students: Vec<String>,
    pub sessions: i32,
    #[serde(default)]
    pub teachers: Vec<String>,
}

impl StudentBodyRaw {
    pub fn new(
        qualification: String,
        students: Vec<String>,
        sessions: i32,
        teachers: Vec<String>,
    ) -> Self {
        Self {
            qualification,
            students,
            sessions,
            teachers,
        }
    }
}
```
