```rust
use crate::TimeSlotRaw;
use serde::Deserialize;
use serde::Serialize;

//For TOML, and i guess JSON, support
#[derive(Deserialize, Serialize)]
pub struct TimetableRaw {
    pub arr: Vec<Vec<crate::timeslot_raw::TimeSlotRaw>>,
}

// Allows you to convert from a vector
impl From<Vec<Vec<TimeSlotRaw>>> for TimetableRaw {
    fn from(arr: Vec<Vec<TimeSlotRaw>>) -> Self {
        TimetableRaw { arr }
    }
}
```
